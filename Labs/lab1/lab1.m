clear 
clf

x = linspace(-1,1,120);
y = exp(1/2-sin(5*pi*x));


plot(x,y, "LineWidth", 2)
print -depsc2 lab1.eps
xlabel("X")
ylabel("y")
ylim([0, 5])
xlim([-0.5, 0.5])
title("Lab 1 Example Figure")
grid on
print -depsc2 lab1.eps
