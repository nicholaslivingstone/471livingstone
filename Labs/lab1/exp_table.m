x = 0:.1:1;
y = [x; exp(x)];
fid = fopen('exp.tex','w');
fprintf(fid,'%.2f & %.8f\\\\\n',y);
fclose(fid);