!
! A simple example: solving an equation f(x) = 0
! using Newton's method
!
! This is the template file used for the scripted version  
!
program newton
  
  implicit none
  double precision :: f,fp,x0,x,dx,Eabs0,Eabs,max_tol,lin,quad
  integer :: iter, k_max

  max_tol = 10E-15  ! Error tolerance
  Eabs0 = 1          ! Current absolute Error
  k_max = 1E5       ! Max number of iterations, stop a 
                    ! non-converging function

  ! Here we try to find the solution to f(x) = 0
  x0 = -0.5d0 ! inital guess / prev iterate's x
  x = x0

  do iter = 1,k_max ! max number of iterations
     f = ffun(x)
     fp = fpfun(x)
     dx = -f/fp
     x = x + dx
     Eabs = abs(x - x0) ! Calculate error
     lin = Eabs / Eabs0             ! Linear Convergance
     quad = Eabs / (Eabs0 * Eabs0)  ! Quadratic Convergence
     write(*,'(I2.2,4(E24.10))') iter, x, Eabs, lin, quad
     if (Eabs < max_tol) then ! Compare error and exit
      exit
     end if
     x0 = x ! Set previous x to current x
     Eabs0 = Eabs
  end do

contains

  double precision function ffun(x)
    implicit none
    double precision :: x

    ffun = sin(x)+cos(x*x)

  end function ffun

  double precision function fpfun(x)
    implicit none
    double precision :: x

    fpfun = cos(x)-2.d0*x*sin(x*x)

  end function fpfun

end program newton