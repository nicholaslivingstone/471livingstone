\documentclass[12pt]{article}

\usepackage{amssymb,amsmath,amsthm}
\usepackage{graphicx} % Package for including figures



\begin{document}

\title{HW2: Convergance of Newton's Method\newline CS471}
\author{Nicholas Livingstone}
\date{\today}
\maketitle

\begin{abstract}
When completing scientific calculations, it's important to understand the accuracy of the tools used to approximate values.
One of these tools commonly used is Newton's Method, an iterative root-finding algorithm. Although it's generally an 
accurate approach to solving functions for zero, the basic instructions for Newton's method can result in inefficient and or
incorrect answers for some equations. This report will analyze cases in which these problems can occur and suggest
modifications in fortran and perl to Newton's method that allows those issues to be resolved. 

\end{abstract}

\section{Introduction}

\subsection{How Newton's method works}
Newton's method outlines approximating the root of a function \(f(x)\) by the following.

\begin{enumerate}
    \item Select a point as an initial guess \(x_n\) on \(f(x)\)
    \item Calculate slope of the point and find the tangent line at the \(x_n\)
    \item Find the root of the tangent line \(x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}\)
    \item Calculate the absolute error as \(E_{abs} =\lvert{x_{n+1}-x_n}\rvert\)
    \item Set the root \(x_{n+1}\) as \(x_n\) and repeat 2-5 until target error is reached
\end{enumerate}

\subsection{General Effectiveness}
This approach is effective for most functions when trying to determine a root. 
However, as we find below, this algorithm runs much more efficiently for some
functions than others. 

\section{Testing Different Functions}

Here, we will be testing the efficiency of newton's method on three different functions:

\begin{enumerate}
    \item \(f(x) = x\)
    \item \(f(x) = x^2\)
    \item \(f(x) = \sin x + \cos x^2\)
\end{enumerate}

For all of these functions \(x_0 = -0.5\) and \(E_{abs}\) target is \(10^{-15}\)

\section{Analysis}

For a linear function \(f(x) = x\), we can clearly see from the data in Table \ref{tab:1} 
that the algorithm operates at O(1) and calculates the root directly.
In the case of \(f(x) = x^2\), the algorithm has a linear convergance at a rate
of 0.5. This results in a very inefficient search for the root, requiring over 40 iterations.
And for \(f(x) = \sin x + \cos x^2\), because there are an infinite number of solutions to
\(f(x)=0\) we cannot determine a consistent convergance as the convergence will depend
on the inital guess of the root. 

\section{Conclusion: Avoiding Linear Convergance}

\(f(x) = x^2\) stands as an outlier among the equations looked at. Newton's method does
not efficienctly find a root. The reason for this is due to the equation having
a multiplicity of 2. Knowing this, we can develop two modifications to the formula used
by newton's method to attempt to produce a quadratic convergence:

\begin{equation}
  x_{n+1} = x_n - m\frac{f(x_n)}{f'(x_n)}
\end{equation}
  
This form fixed-point iteration, where \(m\) is the multiplicity of \(f(x)\), allows linear convergence
to be avoided. Newton's method is said to converge quadratically
when \(g(r) = r\) and \(g'(r) = 0\) where \(g(x) = x - m\frac{f(x)}{f'(x)}\).
Let \(f(x) = x^2\) and \(m = 2\);

\begin{equation}
  \begin{aligned}
    g(x) = x - m\frac{f(x)}{f'(x)}\\
    g(x) = x - m\frac{x^2}{2x}\\
    g(r) = r - m\frac{r^2}{2r}\\
    g(r) = r - mr\\
    g(r) = r(1-r) \neq 0 \\
    g'(r) = '(r(1- r)) = 0
  \end{aligned}
\end{equation}

Since the two hold for this equation, we can conclude that that the algorithm will
converge quadratically using this formula. Therefore, this formula can replace the existing
\(x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}\) in \emph{newtonS.pl} by multiplying the last term
by m. Resulting in \(x_{n+1} = x_n - m\frac{f(x_n)}{f'(x_n)}\). Additionally, this requires
the user to determine the multiplicity of the function before running the algorithm. This could
be hardcoded or added as a parameter when \emph{newtonS.pl} makes a new file from the template. 

\section{Appendix}

\subsection{Code}
\subsubsection{newtonS.pl}
This file tests the convergence for three difference functions using \emph{newtonS.f90.Template}. It
iterates over each line of the Template file and produces a Fortran file specific to a function. It will
then run each file and write the data being outputted. Once the data is written to a file
 for each equation, the code will format each file for use with latex.
\begin{verbatim}
#!/usr/apps/bin/perl
#
# perl program to try the convergence for different functions 
# when using Newton's method for f(x) = 0
# run with : perl newton.pl
#
#
# Here is the generic file
$cmdFile="./newtonS.f90.Template";
$outFile="./newtonS.f90";

# Stuff to converge over

@array_f = ("x", "x*x", "sin(x)+cos(x*x)");
@array_fp = ("1.d0", "2.d0*x", "cos(x)-2.d0*x*sin(x*x)" );

for( $m=0; $m < 3; $m = $m+1){
    # Open the Template file and the output file. 
    open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
    open(OUTFILE,"> $outFile") || die "cannot open file!" ;
    # Setup the outfile based on the template
    # read one line at a time.
    while( $line = <FILE> )
    {
	# Replace the the stings by using substitution
	# s
	$line =~ s/\bFFFF\b/$array_f[$m]/;
	$line =~ s/\bFPFP\b/$array_fp[$m]/;
	print OUTFILE $line;
        # You can always print to secreen to see what is happening.
    print $line;
    }
    # Close the files
    close( OUTFILE );
    close( FILE );
    
    # Run the shell commands to compile and run the program
    system("gfortran $outFile");
    system("./a.out > tmp.txt");

    # Open tmp.txt and modify for a latex format
    $new_file = "./data_output\$m.tex";
    open(FILE, "./tmp.txt") || die "cannot open file!" ;
    open(OUTFILE, '>'."$new_file") || die "cannot open file!" ;
    while( $line = <FILE> )
    {
        $line =~ s/[^\S\r\n]+/ \& /g;
        $line =~ s/\n/\\\\\n/;
        print OUTFILE $line;
    }
    close( OUTFILE );
    close( FILE )
}
exit
\end{verbatim}

\subsubsection{newtonS.f90.Template}

This file is run over by \emph{newtonS.pl} and completes the math to produce the data.
The program utilizes a do loop with a specified number of iterations to prevent an
infinite runtime. The loop will calculate the fixed point form of each iteration and
the absolute error \(E_{abs}\) at each point. If \(E_{abs}\) is lower than the required
threshold, the loop will end and the program will terminate. During each iteration,
data is written to the standard output and \emph{newtonS.pl} will convert it to latex ready
file. 

\begin{verbatim}
!
! A simple example: solving an equation f(x) = 0
! using Newton's method
!
! This is the template file used for the scripted version  
!
program newton
  
  implicit none
  double precision :: f,fp,x0,x,dx,Eabs0,Eabs,max_tol,lin,quad
  integer :: iter, k_max

  max_tol = 10E-15  ! Error tolerance
  Eabs0 = 1          ! Current absolute Error
  k_max = 1E5       ! Max number of iterations, stop a 
                    ! non-converging function

  ! Here we try to find the solution to f(x) = 0
  x0 = -0.5d0 ! inital guess / prev iterate's x
  x = x0

  do iter = 1,k_max ! max number of iterations
     f = ffun(x)
     fp = fpfun(x)
     dx = -f/fp
     x = x + dx
     Eabs = abs(x - x0) ! Calculate error
     lin = Eabs / Eabs0             ! Linear Convergance
     quad = Eabs / (Eabs0 * Eabs0)  ! Quadratic Convergence
     write(*,'(I2.2,4(E24.10))') iter, x, Eabs, lin, quad
     if (Eabs < max_tol) then ! Compare error and exit
      exit
     end if
     x0 = x ! Set previous x to current x
     Eabs0 = Eabs
  end do

contains

  double precision function ffun(x)
    implicit none
    double precision :: x

    ffun = FFFF

  end function ffun

  double precision function fpfun(x)
    implicit none
    double precision :: x

    fpfun = FPFP

  end function fpfun

end program newton
\end{verbatim}

\subsection{Tables/Data}
\begin{table}[h]
  \centering
    \begin{centering}
      \begin{tabular}{| c | c | c | c | c |}
        \hline
        Iteration & \(x\) & \(E_{abs}\) & \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n}\)& \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n^2}\)\\
       \hline 
       \input{Data/data_outputI0.tex}  
       \hline
      \end{tabular}
      \caption{\(f(x) = x\) \label{tab:1}}
      \end{centering}
    \end{table}

\begin{table}[h]
  \centering
    \begin{centering}
      \begin{tabular}{| c | c | c | c | c |}
        \hline
        Iteration & \(x\) & \(E_{abs}\) & \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n}\)& \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n^2}\)\\
       \hline 
       \input{Data/data_outputI1_modified.tex}  
       \hline
      \end{tabular}
      \caption{\(f(x) = x^2\) \label{tab:2}}
      \end{centering}
\end{table}

\begin{table}[h]
  \centering
    \begin{centering}
      \begin{tabular}{| c | c | c | c | c |}
        \hline
        Iteration & \(x\) & \(E_{abs}\) & \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n}\)& \(\frac{(E_{abs})_{n+1}}{(E_{abs})_n^2}\)\\
       \hline 
       \input{Data/data_outputI2.tex}  
       \hline
      \end{tabular}
      \caption{\(f(x) = \sin x + \cos x^2\) \label{tab:3}}
      \end{centering}
\end{table}


\end{document}