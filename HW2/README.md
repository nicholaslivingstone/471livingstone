# HW 2 README
## Nicholas Livingstone CS471 Spring 2020


- The homework report is in the directory HW2/Report and is called *hw2 report.pdf*
- The code for this homework is located in
  HW2/Code and is documented in the appendix
  of the report.
- To reproduce data in report:
1. In terminal run `perl newtonS.pl`
2. The script will run through three iterations of *newtonS.f90.Template* for each equation found in the report.
3. Data will be produced in text files ready for latex in the form *data_outputI**x**.tex* where **x** is the equation used to produce the data

Note: The latex file uses a modified form of the data file for the second equation f(x) = x^2, found as: *data_outputI1_modified*. This was done by hand to avoid presenting too much data in the report. 